public class SameFirstLast {
    public boolean sameFirstLast(Integer[] nums) {
        return  (nums.length != 0 && (nums[0] == nums[nums.length-1]));
    }

    public static void main(String[] args) {
        SameFirstLast compareElements = new SameFirstLast();
        Integer[] testArray = {4,2,3,4,5,4};
        System.out.println(compareElements.sameFirstLast(testArray));

    }
}
