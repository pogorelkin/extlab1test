public class CountHI {
    String hi = "hi";
    String  substring = "";
    int sum = 0;
    public CountHI(String str) {
        for (int i = 0; i < str.length() - 1; i++) {
            substring = str.substring(i, i+2);
            if (substring.compareToIgnoreCase(hi) == 0)
                this.sum++;
        }
        System.out.println("Number of 'Hi' in string is equal to " + sum);
    }

    public static void main(String[] args) {
        String stroka = "hiHIhaihaihihi"; //4;
        CountHI count = new CountHI(stroka);
    }
}
