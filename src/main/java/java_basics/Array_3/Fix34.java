import java.util.Arrays;
public class Fix34 {
    public void fix34(Integer[] nums) {
        int temp;
        for (int i = nums.length-1; i > 1; i--) //decr
           if ((nums[i] == 4) && (nums[i-1]) != 3) {                 //find 4
            temp = nums[i];                                          // if prev element isnt equal to 3 then swap them
            nums[i] = nums[i-1];
            nums[i-1] = temp;
           }
        System.out.println(Arrays.toString(nums));
    }

    public static void main(String[] args) {
        Integer[] testArray = {1,3,13,13,5,2,2,2,1,4,8,9,5};
        System.out.println(Arrays.toString(testArray) + " origin array");
        Fix34 ara = new Fix34();
        ara.fix34(testArray);
    }
}
//done
