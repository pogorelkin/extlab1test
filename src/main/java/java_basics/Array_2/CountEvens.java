public class CountEvens {
    public int countEvens(Integer[] nums) {
        int counter = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % 2 == 0)
                counter++;
        }

        return counter;
    }

    public static void main(String[] args) {
        Integer[] testArray = {4,2,3,4,5,4,2,2,2,1,1,1,8,9,10};
        CountEvens ara = new CountEvens();
        System.out.println(ara.countEvens(testArray));
    }
}
